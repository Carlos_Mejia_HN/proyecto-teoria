window.onload = () => {
    main();
}

this.main = () => {
    let btnCalcular = document.getElementById('btnCalcular');
    this.atachEvents();
    this.precios = [];
    this.labels = [];
}

this.atachEvents = () => {
    var _self = this;
    btnCalcular.addEventListener('click',function(){
        let _dataset = datasetGasolinaSuper();
        let _dataset2 = datasetGasolinaRegular();
        let _dataset3 = datasetDiesel();
        let precioInicialSuper = _dataset[0];
        let precioFinalSuper = _dataset[_dataset.length - 1];
        let factorAumentoSuper = ( precioFinalSuper - precioInicialSuper ) / (_dataset.length - 1);
        let precioInicialRegular = _dataset2[0];
        let precioFinalRegular = _dataset2[_dataset2.length - 1];
        let factorAumentoRegular = ( precioFinalRegular - precioInicialRegular ) / (_dataset2.length - 1);
        let precioInicialDiesel = _dataset3[0];
        let precioFinalDiesel = _dataset3[_dataset3.length - 1];
        let factorAumentoDiesel = ( precioFinalDiesel - precioInicialDiesel ) / (_dataset3.length - 1);
        let fechaActual = new Date();
        let fechaDestino = new Date(document.getElementById('fechaTarget').value);
        if(fechaDestino && fechaDestino > fechaActual){
            let diferenciaDeFechas = fechaDestino - fechaActual;
            let diferenciaFechasEnSemanas = Math.floor(diferenciaDeFechas / (1000* 60 * 60 * 24 * 7));
            let ObjectToChartSuper = _self.generateObjectsChart(diferenciaFechasEnSemanas,precioFinalSuper,factorAumentoSuper,fechaActual);
            let ObjectToChartRegular = _self.generateObjectsChart(diferenciaFechasEnSemanas,precioFinalRegular,factorAumentoRegular,fechaActual);
            let ObjectToChartDiesel = _self.generateObjectsChart(diferenciaFechasEnSemanas,precioFinalDiesel,factorAumentoDiesel,fechaActual);
            const dataSource = {
                "chart": {
                  "caption": "Precios de las gasolinas en Honduras",
                  "yaxisname": "Fecha",
                  "subcaption": "Proyección",
                  "showhovereffect": "1",
                  "numbersuffix": "Lps.",
                  "drawcrossline": "1",
                  "plottooltext": "Cantidad de Lps. <b>$dataValue</b> para $seriesName",
                  "theme": "fusion"
                },
                "categories": [
                  {
                    "category": ObjectToChartSuper.labels.map(function(label){
                        return {"Label":label};
                    })
                  }
                ],
                "dataset": [
                  {
                    "seriesname": "Gasolina Super",
                    "data": ObjectToChartSuper.precios
                  },
                  {
                    "seriesname": "Gasolina Regular",
                    "data": ObjectToChartRegular.precios
                  },
                  {
                    "seriesname": "Diesel",
                    "data": ObjectToChartDiesel.precios
                  }
                ]
            };
            FusionCharts.ready(function() {
                var myChart = new FusionCharts({
                   type: "msline",
                   renderAt: "chart-container",
                   width: "90%",
                   height: "90%",
                   dataFormat: "json",
                   dataSource
                }).render();
             });        
        }
        else{
            alert('Seleccione una fecha mayor a la fecha actual');
        }
    });
}

this.generateObjectsChart = (diferenciaEnSemanas,precioActual,factorAumento,fechaActual) => {
    let labels = [];
    let precios = [];
    for(var i = 0; i < diferenciaEnSemanas ; i++){
        labels.push(fechaActual.getDate() +'/'+ (fechaActual.getMonth() + 1) +'/'+ fechaActual.getFullYear());
        precios.push({
            "value": precioActual
        });
        precioActual = precioActual + factorAumento;
        fechaActual.setDate(fechaActual.getDate() + 7);
    }
    return {labels,precios};
}