function datasetGasolinaSuper(dataset){
    if(dataset && Array.isArray(dataset)){
        this.dataset = dataset;
    } 
    if (this.dataset && Array.isArray(this.dataset)) {
        return this.dataset
    } else {
        return [
            91.97,
            92.39,
            93.64,
            94.66,
            96.02,
            96.89,
            97.27,
            96.74,
            96.21,
            94.74,
            94.28,
            95.19,
            95.84,
            96.48,
            96.48,
            97.2,
            97.46,
            98.52,
            99.32,
            100.42,
            101.32,
            101.55,
            101.89,
            101.81,
            101.21,
            100.55,
            100.35,
            100.45,
            100.42,
            100.53,
            100.9,
            100.68,
            100.68,
            100.68,
            100.68,
            100.64,
            100.79,
            101.29,
            101.89,
            102.8,
            103.48,
            103.33,
            103.03,
            101.82,
            98.83,
            96.74,
            95.07,
            93.3,
            92.2       
        ];
    }
}