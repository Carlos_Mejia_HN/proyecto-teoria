window.onload = () => {
    main();
}

this.main = () => {

    let _datasetGasolinaSuper = datasetGasolinaSuper();
    let average = 0;
    let arrayVariations = [];
    let result = 0;
    let maxDifferential = 0; 
    let minDifferential = 0;
    let lastFive = this.lastFive(_datasetGasolinaSuper);
    let lastFiveDifferential = this.differential(lastFive); 
    let lastFiveDifferentialAverage = this.average(lastFiveDifferential);
    let btnCalcularHonduras = document.getElementById('btnCalcularHonduras');
    let btnEjecucionPersonalizada = document.getElementById('btnEjecucionPersonalizada');
    let btnGuardarArchivo = document.getElementById('btnGuardarArchivo');
    this.file = document.getElementById('btnArchivo');
    this.csvArray = [];

    this.atachEvents();
    average = this.average(_datasetGasolinaSuper);

    arrayVariations = this.differential(_datasetGasolinaSuper);

    maxDifferential = this.getMax(arrayVariations);

    minDifferential = this.getMin(arrayVariations);

    if (average > 0) {
        result = average + lastFiveDifferentialAverage;
        console.log(`Predicción: ${result}, margen de error: +${maxDifferential} y ${minDifferential}`);
    } else {
        throw Error('Ocurrió un error en el cálculo');
    }
}


this.average = (array) => {
    let length = array.length;
    let sum = 0;
    if (Array.isArray(array)) {
        array.forEach( (element) => {
            if (Number(element) !== NaN) {
                sum += element;
            }
        });
        return Number((sum / length).toFixed(2));
    }
    return null;
}


this.differential = (array) => {
    let length = array.length;
    let arrayVariations = [];
    let diff = 0;
    if (Array.isArray(array)) {
        array.forEach( (element, index) => {
            if (Number(element) !== NaN) {
                diff = element - array[index - 1] || 0;
                arrayVariations.push(Number(diff.toFixed(2)));
            }
        });
    }
    return arrayVariations;
}


this.getMax = (array) => {
    let max = Math.max.apply(null, array);
    return max;
}


this.getMin = (array) => {
    let min = Math.min.apply(null, array);
    return min;
}


this.lastFive = (array) => {
    let lastFive = [];
    if (Array.isArray(array)) {
        lastFive = array.filter( (element, index) => index > (array.length - 5) ? element : null );
    }
    return lastFive;
}


this.atachEvents = () => {
    if(btnCalcularHonduras){
        btnCalcularHonduras.addEventListener('click', function(){
            window.location.href = 'calculoHonduras.html';
        });
    }
    if(btnGuardarArchivo){
        btnGuardarArchivo.addEventListener('click', () => {
            if (this.file.files[0]) {
                // console.log(this.file.files[0]);
                this.fileReader(this.file.files[0]);
            }
        });
    }
}


this.csvToArray = (csv) => {
    let array = [];
    let rows = csv.split("\n");
    rows.forEach( (row) => {
        row = row.replace(/\s/g, '');
        array.push(Number(row));
    });
    return array;
};


this.fileReader = (file) => {
    let reader = new FileReader();

    // Closure to capture the file information.
    reader.onload = ( (theFile) => {
        return (e) => {
            let array = this.csvToArray(e.target.result);
            datasetGasolinaSuper(array);
            window.location.href = 'calculoHonduras.html';
            console.log(`${array}`);
        };
    })(file);

    reader.readAsText(file);
}